#!/bin/sh
#remove these artwork files that include the trademarked/copyrighted images
#also merge the cursors with this artwork to avoid an unnecessary debian package

filex=$1
cursors="http://git.byteme.org.uk/cursors.tar.gz"
bitmap="http://git.byteme.org.uk/omvviewer.BMP"

version=`echo "$filex" | sed  -r 's/(.*-)([0-9]*\.[0-9]*\.[0-9]*)(-r.*)/\2/'`
temp=`mktemp -d`

cp $filex $temp
curdir=`pwd`
cd $temp
unzip *.zip
wget $cursors
tar -xvzf cursors.tar.gz
cp indra linden/ -r

wget $bitmap
cp omvviewer.BMP linden/indra/newview/res-sdl/

rm linden/indra/newview/res/bitmap2.bmp
rm linden/indra/newview/res/install_icon.BMP
rm linden/indra/newview/res/ll_icon.BMP
rm linden/indra/newview/res/ll_icon.png
rm linden/indra/newview/res/loginbackground.bmp
rm linden/indra/newview/res/uninstall_icon.BMP
rm linden/indra/newview/res-sdl/ll_icon.BMP

ls -l

tar -cf omvviewer-artwork-$version+dfsg.tar linden/
gzip -9 omvviewer-artwork-$version+dfsg.tar
cp omvviewer-artwork-$version+dfsg.tar.gz $curdir

rm $temp -r --force

